// remove: import logo from './logo.svg';
import './App.css';
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import { Container } from 'react-bootstrap';
import {UserProvider} from './UserContext';
import {useState} from 'react';


/* React Fragments allows us to return multiple elements*/
// import { Fragment } from 'react'

import {BrowserRouter as Router } from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'

function App() {

  const[user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    /* React Fragments allows us to return multiple elements*/
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>

      <Container fluid>
        <AppNavbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/Courses" element={<Courses />} />
          <Route path="/Register" element={<Register />} />
          <Route path="/Login" element={<Login />} />
          <Route path="/Logout" element={<Logout />} />
          <Route path="*" element={<Error />} />
          
        </Routes>
      </Container>
    </ Router>
    </UserProvider>
    
  );
}

export default App;


 // /*
 //        <Router>
 //          <Routes>
 //            <Route path="/yourDesiredPath" element { <Component /> } />
 //          </Routes>
 //        </Router>
 //      */


 //      /*{
 //        <div class="container">
 //        </div>
 //        }
 //   */