import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useContext } from 'react';
import UserContext from '../UserContext'
import { Navigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login(){

    // 3 
    // variable, setter function
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);


    // console.log(email);
    // console.log(password);


    // function loginUser(event){

    //     // Prevents page redirection via form submission
    //     event.preventDefault();

    //     // Clear input fields after submission
    //     localStorage.setItem('email', email);

    //     setUser({
    //         email: localStorage.getItem('email')
    //     })

    //     setEmail('');
    //     setPassword('');
    //     alert('You are now logged in.');
    // }

    function authenticate(e){
        e.preventDefault();

        fetch('http://localhost:4000/users/login',{
             method:'POST',
             headers: {'Content-type' : 'application/json'},
             body: JSON.stringify({
                 email: email,
                 password: password
             })

        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken");
            console.log(data.accessToken);
            

            if(typeof data.sccessToken !== "underfined"){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.sccessToken);


             Swal.fire({
                title: "Login Succesful",
                icon: "success",
                text: "Welcome to Zuitt!"
            })
            }
        else{
            Swal.fire({
                title: "Authenticate failed",
                icon: "error",
                text: "Check your login details and try again."
            })
        }
        })


        setEmail('');
    }
   
   const retrieveUserDetails =(token) => {
       fetch ('http://localhost:4000/users/details',{
          headers: {
            Authorization: 'Bearer ${token}'
          }
       })
       .then(res => res.json)
       .then(data =>{
         console.log(data);

         setUser({
            id: data._id,
            isAdmin: data.isAdmin
         })
       })
   }

    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password])

 
    return(
        (user.id !== null)
        ? // true - means email field is successfully set
        <Navigate to="/courses" />
        :
        <Form onSubmit={(e) => authenticate(e)} >
            <Form.Group controlId="userEmail">
                <h3>Login</h3>
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group><br/>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value = {password}
                    onChange = {event => setPassword(event.target.value)}
                    required
                />
            </Form.Group><br/>
            { isActive ?

            <Button variant="primary" type="submit" id="submitBtn">
                Login
            </Button>
            : 
            <Button variant="primary" type="submit" id="submitBtn">
                Login
            </Button>
              }

        </Form>
    )
}