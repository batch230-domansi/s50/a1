
import Banner from '../components/Banner';

export default function Error() {
	const data = {
	title : "404 - NOT FOUND",
	content: "The page you are looking for cannot be founs", 
	destination: "/",
	label: "Back Home"
}


	return (
		<Banner data = {data}/>
	)
}