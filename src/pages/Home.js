import { Fragment } from 'react';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


const data = {
	title : "Zuitt Coding Bootcamp",
	content: "Opportunities for everyone, everwhere", 
	destination: "/",
	label: "Enrol now!"
}

export default function Home() {

	return (
		<Fragment>
			<Banner data = {data} />
			<Highlights />
		</Fragment>
	)
}