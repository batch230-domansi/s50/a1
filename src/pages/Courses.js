import { Fragment } from 'react';
import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'

export default function Courses() {
	console.log("Contents of coursesData: ")
	console.log(coursesData);
	console.log(coursesData[0]);

	const courses = coursesData.map(course => {

		return (
           <CourseCard key ={course.id} courseProp={course} />
			)
	})

	// return(
	// 	<Fragment>
	// 		<CourseCard courseProp={coursesData[0]} />
	// 	</Fragment>
		
	// )

	return(
       <Fragment>
           {courses}
       </Fragment>
		)
};