import{Container, Navbar, Nav} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useState, Fragment, useContext} from 'react';
import UserContext from "../UserContext";


export default function AppNavbar(){
	//const [user,setUser] = useState(localStorage.getItem("email"));

    const {user} = useContext(UserContext);
	console.log("result of getting item from localstorage: ")
	console.log(user);

	return(
		<Navbar bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand href="#home">Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar id="basic-navbar-nav">
		            <Nav className="mr-auto">
			            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
			            <Nav.Link as={NavLink} to="/Courses">Courses</Nav.Link>
			            {(user.id !== null)?
			            	<Nav.Link as={NavLink} to="/Logout">Logout</Nav.Link>
			            	:
			            	<Fragment>
				            	<Nav.Link as={NavLink} to="/Login">Login</Nav.Link>
				            	<Nav.Link as={NavLink} to="/Register">Register</Nav.Link>
			            	</Fragment>
			        	}
		            </Nav>
		        </Navbar>
		    </Container>
		</Navbar>
	)
}