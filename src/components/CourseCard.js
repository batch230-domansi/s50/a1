import { Card, Button } from 'react-bootstrap';
import {useState, useEffect } from 'react';
import PropTypes from 'prop-types';

/*export default function CourseCard(props) {

  console.log("Contents of props: ");
  console.log(props);
  console.log(typeof props);

  // return (
  //     <Card>
  //         <Card.Body>
  //             <Card.Title>Sample Course</Card.Title>
  //             <Card.Subtitle>Description:</Card.Subtitle>
  //             <Card.Text>This is a sample course offering.</Card.Text>
  //             <Card.Subtitle>Price:</Card.Subtitle>
  //             <Card.Text>PhP 40,000</Card.Text>
  //             <Button variant="primary">Enroll</Button>
  //         </Card.Body>
  //     </Card>
  // )
   return (
      <Card>
          <Card.Body>
              <Card.Title>{props.courseProp.name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{props.courseProp.description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{props.courseProp.price}</Card.Text>
              <Button variant="primary">Enroll</Button>
          </Card.Body>
      </Card>
  )
}*/

// second way 

/*export default function CourseCard ({courseProp}) {

   return (
      <Card>
          <Card.Body>
              <Card.Title>{courseProp.name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{courseProp.description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{courseProp.price}</Card.Text>
              <Button variant="primary">Enroll</Button>
          </Card.Body>
      </Card>
  )
}*/

//third way 

export default function CourseCard ({courseProp}) {

  const {name, description, price} = courseProp;
  // getter, setter
  // variable, function to change the value of a variable
  const [count, setCount]= useState(0); //count =0;
  const [seats, setSeats] = useState(30);

  function enroll() {
/*
    if (seats > 0) {
        setCount(count + 1);
        console.log('Enrollees: ' + count);
        setSeats(seats - 1);
        console.log('Seats: ' + seats);
    } else {
      alert('No more seats available.');
    }
    */

    setCount(count + 1);
    console.log('Enrollees: ' + count);
    setSeats(seats - 1);
    console.log('Seats: ' + seats);
  }

  useEffect(() => {
    if(seats === 0){
      alert('No more seats available');
    }
  }, [seats]);

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Button variant="primary" onClick={enroll}>Enroll</Button>
        <Card.Text>Total Enrolled: {count} <br/></Card.Text>
      </Card.Body>
    </Card>
  )
}


CourseCard.propTypes ={
  courseProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}
